from django.db import models

# Create your models here.
class BankRate(models.Model):
	hui_in = models.DecimalField(blank=True, max_digits=10, decimal_places=4,null=True, default=6.0000)
	hui_out = models.DecimalField(blank=True, max_digits=10, decimal_places=4,null=True, default=6.0000)
	chao_in = models.DecimalField(blank=True, max_digits=10, decimal_places=4,null=True, default=6.0000)
	chao_out = models.DecimalField(blank=True, max_digits=10, decimal_places=4,null=True, default=6.0000)
	name = models.CharField(max_length=10, null=False, blank=False)
	code = models.CharField(max_length=10, null=False, blank=False)
	created = models.DateTimeField(auto_now_add=True)

	class Meta:
		db_table = "bankrates"

	def __str__(self):
		return "{0},created:{1}".format(self.name,self.created.strftime("%Y/%m/%d %H:%M:%S"))