from django.contrib import admin
from .models import BankRate

# Register your models here.

class BankRateAdmin(admin.ModelAdmin):
	fields=("hui_in","hui_out","chao_in","chao_out","code",)
	list_display = ("hui_in","hui_out","chao_in","chao_out","code",)


admin.site.register(BankRate,BankRateAdmin)