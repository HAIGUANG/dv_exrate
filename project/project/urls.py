
from django.contrib import admin
from django.urls import path,include
from django.conf.urls.static import static
from django.conf import settings
from django.views.generic import TemplateView

from rest_framework_jwt.views import obtain_jwt_token,refresh_jwt_token,verify_jwt_token
from rest_framework import routers
from useraccount.viewsets import UserProfileViewSet
from mail_exchange.viewsets  import OrderViewSet,OfferViewSet

from rest_framework_swagger.views import get_swagger_view
schema_view = get_swagger_view(title='Pastebin API')



router = routers.DefaultRouter()
router.register('userprofiles', UserProfileViewSet)
router.register('order', OrderViewSet)
router.register('offer', OfferViewSet)



urlpatterns = [
    path('admin/', admin.site.urls),
    path('auth/', include('djoser.urls')),
    path('auth/', include('djoser.urls.authtoken')),
    path('auth/', include('djoser.urls.jwt')),

    path('api/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls')),
    path('api-token-auth/', obtain_jwt_token),
    path('api-token-refresh/', refresh_jwt_token),
    path('api-token-verify/', verify_jwt_token),

    path('admin/statuscheck/', include('celerybeat_status.urls')),


    path('accounts/', include('useraccount.urls')),
    path('accounts/', include('allauth.urls')),
    # path('orders/', include('exrate.urls')),
    path('openAPIS/', schema_view),

    path('vue/', TemplateView.as_view(template_name="vue.html")),
    path('test/', TemplateView.as_view(template_name="vues/offers.html")),
    path('orders/', TemplateView.as_view(template_name="orders/detail.html")),
    path('chat/', include('chat.urls')),

    path('', include('musics.urls')),
]+ static(settings.STATIC_URL,document_root=settings.STATIC_ROOT)+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)