from django.apps import AppConfig


class EnvSystemConfig(AppConfig):
    name = 'env_system'
