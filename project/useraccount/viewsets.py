from django.shortcuts import render

# Create your views here.
from django.contrib.auth.models import User
from .models import UserProfile,fun_sql_cursor_update
from .serializers import UserProfileSerializerV1, UserProfileSerializer,UserSerializer

from rest_framework import viewsets,status
from rest_framework.permissions import IsAuthenticated,BasePermission,SAFE_METHODS
from rest_framework.decorators import list_route,detail_route,permission_classes,api_view
from rest_framework.response import Response
# from rest_framework.views import APIView
from django.http.request import HttpRequest
import logging


logger=logging.getLogger("error_logger")

class ReadOnly(BasePermission):
    def has_permission(self, request, view):
        return request.method in SAFE_METHODS


# Create your views here.
class UserProfileViewSet(viewsets.ModelViewSet):
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializerV1
    # permission_classes = (IsAuthenticated,) 
    # permission_classes = (IsAuthenticated|ReadOnly,) #Object Level permission

    # def get_permissions(self):
    #     if self.action in ('create',):
    #         self.permission_classes = [IsAuthenticated]
    #     return [permission() for permission in self.permission_classes]

    def create(self,request):
        file=request.FILES['avatar']
        path= os.path.join("/media/avatarts/",file.name)
        destination = open(path, "wb")

        for chunk in file.chunks():
            destination.write(chunk)
        destination.close()

        if not os.path.exists(path):
            print('File not found:', path)
            return create_render(request)

        userprofile, created = UserProfile.objects.get_or_create(avatar=path)
        if created:
            # image.sender = request.POST['sender']
            userprofile.organization = request.POST['organization']
            userprofile.telephone = request.POST['telephone']
            image.save()

        return Response({'message': 'OK'})



    @list_route(methods=['get'])
    def test_lionhu(self,request,format=None):
        content={
            "status":"lionhu know"
        }

        return Response(content)

    # @permission_classes(IsAuthenticated)
    # @list_route(methods=['get'])
    # def raw_sql_query(self, request):
    #     song = request.query_params.get('song', None)
    #     music = fun_raw_sql_query(song=song)
    #     serializer = MusicSerializer(music, many=True)
    #     return Response(serializer.data, status=status.HTTP_200_OK)


    # /api/music/{pk}/sql_cursor_update/
    # @permission_classes(IsAuthenticated)
    @detail_route(methods=['put'])
    def sql_cursor_update(self, request, pk=None):
        organization = request.data.get('organization', None)
        telephone = request.data.get('telephone', None)
        if organization:
            userprofile = fun_sql_cursor_update(organization=organization,telephone = telephone, pk=pk)
            return Response(userprofile, status=status.HTTP_200_OK)

    # /api/music/version_api/
    # within head set following
                # Content-Type: application/json
                # Accept: application/json; version=1.0
    @list_route(methods=['get'])
    def ver_api(self, request):
        profiles=UserProfile.objects.all()
        if self.request.version == '1.0':
            serializer = UserProfileSerializerV1(profiles, many=True)
        else:
            serializer = UserProfileSerializer(profiles, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @detail_route(methods=['get'])
    def childrens(self,request, pk=None):

        logger.error("childrens")
        user = User.objects.get(pk=pk)
        
        logger.error(pk)
        logger.error(user)
        logger.error(user.profile.partner_id)

        if user and user.id == user.profile.partner_id:
            childrens = User.objects.filter(profile__partner_id = user.id)
            serializer = UserSerializer(childrens,many=True)
            return Response(serializer.data,status=status.HTTP_200_OK)

        if user and user.id == user.profile.grandfather_id:
            childrens = User.objects.filter(profile__grandfather_id_id = user.id)
            serializer = UserSerializer(childrens,many=True)
            return Response(serializer.data,status=status.HTTP_200_OK)

        if user and user.id == user.profile.father_id:
            childrens = User.objects.filter(profile__father_id_id = user.id)
            serializer = UserSerializer(childrens,many=True)
            return Response(serializer.data,status=status.HTTP_200_OK)

        content = {'message': 'no child'}

        return Response(content,status=status.HTTP_404_NOT_FOUND)
