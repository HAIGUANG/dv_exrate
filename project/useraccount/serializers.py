from rest_framework import serializers
from .models import UserProfile
from django.utils.timezone import now
from django.contrib.auth.models import User


class UserProfileSerializer(serializers.ModelSerializer):
   # singer_song = serializers.SerializerMethodField()


   class Meta:
       model = UserProfile
       # fields = '__all__'
       fields = ('organization',"avatar", 'telephone', )

   # def get_singer_song(self, obj):
   #     return obj.singer+':  '+obj.song


class ToUpperCaseCharField(serializers.CharField):
    def to_representation(self, value):
        return value.upper()


class UserSerializer(serializers.ModelSerializer):
    profile = UserProfileSerializer(read_only=True)
    profile_id = serializers.PrimaryKeyRelatedField(queryset=UserProfile.objects.all(), write_only=True)
    class Meta:
        model = User
        fields = ( "id", "username","profile","profile_id")



class UserProfileSerializerV1(serializers.ModelSerializer):
   days_since_created = serializers.SerializerMethodField()
   organization = ToUpperCaseCharField()
   father = UserSerializer(read_only=True)
   father_id = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), write_only=True)
   grandfather = UserSerializer(read_only=True)
   grandfather_id = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), write_only=True)
   partner = UserSerializer(read_only=True)
   partner_id = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), write_only=True)

   class Meta:
       model = UserProfile
       fields = ('id','organization', 'telephone',"avatar",'days_since_created',
        'father','father_id','grandfather','grandfather_id','partner','partner_id')

   def get_days_since_created(self, obj):
       return (now() - obj.mod_date).days

