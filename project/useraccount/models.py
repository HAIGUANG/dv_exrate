import uuid
import os
from django.utils import timezone
from django.db import models,connection
from django.contrib.auth.signals import user_logged_in,user_logged_out
from django.dispatch import receiver
from collections import namedtuple
from django.contrib.auth.models import User
from allauth.account.models import EmailAddress

from imagekit.models import ImageSpecField, ProcessedImageField
from imagekit.processors import ResizeToFill
import uuid

# Create your models here.

MEMBERSHIP_CHOICES=(
        ("B","Bronze"),
        ("S","Silver"),
        ("D","Diamond"),
        ("P","Partner"),
    )

def namedtuplefetchall(cursor):
    # Return all rows from a cursor as a namedtuple
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]


def fun_sql_cursor_update(**kwargs):
    organization = kwargs.get('organization')
    telephone = kwargs.get('telephone')
    pk = kwargs.get('pk')

    '''
    Note that if you want to include literal percent signs in the query, 
    you have to double them in the case you are passing parameters:
    '''
    with connection.cursor() as cursor:
        cursor.execute("UPDATE useraccount_userprofile SET organization = %s,  telephone = %s WHERE id = %s", [organization, telephone, pk])
        cursor.execute("SELECT profile.*, auth_user.username FROM useraccount_userprofile as profile LEFT JOIN auth_user  ON profile.user_id = auth_user.id WHERE profile.id = %s", [pk])
        # result = cursor.fetchone()
        result = namedtuplefetchall(cursor)

    result = [
        {
            'id': r.id,
            'user': r.username,
            'organization': r.organization,
            'telephone': r.telephone
        }
        for r in result
    ]

    return result

def get_image_path(instance, filename):
    # prefix = 'avatars/'
    prefix = "{}/avatars/".format(instance.user.id)
    name = str(uuid.uuid4()).replace('-', '')
    extension = os.path.splitext(filename)[-1]
    return prefix + name + extension

def get_ID_image_path(instance,filename):
    prefix = "{}/IDimages/".format(instance.user.id)
    name = str(uuid.uuid4()).replace("-","")
    extension = os.path.splitext(filename)[-1]

    return prefix + name + extension
    return os.path.join(prefix,name,extension)


def get_thumbnail_path(self, filename):
    prefix = 'avatars/thumbnail'
    name = str(uuid.uuid4()).replace('-', '')
    extension = os.path.splitext(filename)[-1]
    return prefix + name + extension

def get_default_ancestor():
    root = User.objects.get(username="root")
    return root if root else null


class UserProfile(models.Model):
    slug = models.SlugField(null=True,blank=True,default=uuid.uuid4())
    user = models.OneToOneField(User,on_delete=models.CASCADE, related_name="profile")
    father = models.ForeignKey(User,on_delete=models.SET(get_default_ancestor), blank=True, null=True, related_name="sons")
    grandfather = models.ForeignKey(User,on_delete=models.SET(get_default_ancestor), blank=True, null=True, related_name="grandsons")
    partner = models.ForeignKey(User,on_delete=models.SET(get_default_ancestor), blank=True, null=True, related_name="decendants")
    organization = models.CharField('Organization',max_length=128,blank=True)
    membership = models.CharField(choices=MEMBERSHIP_CHOICES, max_length=5,default="B")
    tradesum_sons = models.IntegerField(default=0)
    tradesum_grandsons = models.IntegerField(default=0)
    telephone = models.CharField("Telephone",max_length=50,blank=True)
    avatar = models.ImageField(upload_to=get_image_path,default="new.jpg", blank=True, null=True)
    thumbnail = ImageSpecField(source='avatar',
                            processors=[ResizeToFill(250,250)],
                            format="PNG",
                            options={'quality': 60}
                            )
    id_image= models.ImageField(upload_to=get_ID_image_path,default="id.png",blank =True,null=True)

    mod_date = models.DateTimeField('Last modified',auto_now=True)

    class Meta:
        verbose_name:"User Profile"

    def __str__(self):
        return "{}'s profile".format(self.user.__str__)

    def account_verified(self):
        if self.user.is_authenticated:
            result = EmailAddress.objects.filter(email = self.user.email)
            if len(result):
                return result[0].verfied
        return False

    def sons_sum(self):
        return User.objects.filter(father = self.id).count()

    def grandsons_sum(self):
        return User.objects.filter(grandfather = self.id).count()

    # @delete_previous_file
    # def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
    #     super(UserProfile, self).save()

    # @delete_previous_file
    # def delete(self, using=None, keep_parents=False):
    #     super(UserProfile, self).delete()



class VisitHistory(models.Model):
    user = models.ForeignKey(User,verbose_name="user",on_delete=models.CASCADE)
    login_time = models.DateTimeField("login_time",blank=True,null=True)
    logout_time = models.DateTimeField("logout_time",blank=True,null=True)

    def __str__(self):
        # login_dt=timezone.localtime(self.login_time)
        # return '{0} - {1.year}/{1.month}/{1.day} {1.hour}:{1.minute}:{1.second} - {2}'.format(
        #     self.user.username, login_dt, self.get_diff_time()
        # )
        return '{0} - {1}'.format(
            self.user.username, self.get_diff_time()
        )

    def get_diff_time(self):
        if not self.logout_time:
            return 'still logining'
        else:
            td = self.logout_time - self.login_time
            return '{0}時間{1}分'.format(
                td.seconds // 3600, (td.seconds // 60) % 60)

@receiver(user_logged_in)
def user_logged_in_callback(sender, request, user, **kwargs):
    VisitHistory.objects.create(user=user, login_time=timezone.now())


@receiver(user_logged_out)
def user_logged_out_callback(sender, request, user, **kwargs):
    records = VisitHistory.objects.filter(user=user, logout_time__isnull=True)
    if records:
        record = records.latest('pk')
        record.logout_time = timezone.now()
        record.save()






