from .models import Order,Offer
from .serializers import OrderSerializer,OfferSerializer
from rest_framework import viewsets, permissions


class OrderViewSet(viewsets.ModelViewSet):
    """ViewSet for the Bill class"""

    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = [permissions.IsAuthenticated]

class OfferViewSet(viewsets.ModelViewSet):
    """ViewSet for the Bill class"""

    queryset = Offer.objects.all()
    serializer_class = OfferSerializer
    permission_classes = [permissions.IsAuthenticated]
