from .models import Order,Offer

from rest_framework import serializers
from useraccount.serializers import UserSerializer
from django.contrib.auth.models import User
import uuid

class OrderSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only = True)
    user_id = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), write_only=True)

    def create(self, validated_data):
        validated_data['user'] = validated_data.get('user_id', None)

        if validated_data['user'] is None:
            raise serializers.ValidationError("user not found.") 

        del validated_data['user_id']

        validated_data['slug'] = str(uuid.uuid4())
        return Order.objects.create(**validated_data)

    def update(self, instance, validated_data):

        validated_data['user'] = validated_data.get('user_id', None)

        if validated_data['user'] is None:
            raise serializers.ValidationError("user not found")

        instance.user = validated_data['user']
        instance.status = "updated"
        instance.amount = validated_data.get("amount",instance.amount)
        instance.save()
        return instance


    class Meta:
        model = Order
        fields = (
            'id',
            'slug',
            'user_id',
            "user",
            'created',
            "status",
            'last_updated',
            'amount',
            'from_currency',
            'to_currency',
            'due_at',
            'rate',
            'active',
            'privacy',
            'send_notification',
        )

class OfferSerializer(serializers.ModelSerializer):
    follower = UserSerializer(read_only = True)
    follower_id = serializers.PrimaryKeyRelatedField(queryset=User.objects.all(), write_only=True)
    order = OrderSerializer(read_only = True)
    order_id = serializers.PrimaryKeyRelatedField(queryset=Order.objects.all(), write_only=True)
    
    def create(self, validated_data):
        validated_data['follower'] = validated_data.get('follower_id', None)
        validated_data['order'] = validated_data.get('order_id', None)

        if validated_data['follower'] is None :
            raise serializers.ValidationError("follower not found.") 

        del validated_data['follower_id']

        if validated_data['order'] is None :
            raise serializers.ValidationError("order not found.") 

        # del validated_data['order']
        del validated_data['order_id']

        validated_data['slug'] = str(uuid.uuid4())

        return Offer.objects.create(**validated_data)

    class Meta:
        model = Offer
        fields = (
            'slug', 
            'follower_id',
            "follower",
            'order_id',
            "order",
            'created', 
            'last_updated', 
            'price', 
            'due_at', 
            'send_notification', 
            'status', 
        )
