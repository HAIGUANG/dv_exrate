# Generated by Django 2.2.2 on 2019-07-28 10:33

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('slug', models.SlugField(blank=True, default=uuid.UUID('4c736e09-719e-4c56-80f2-9275ce6f9825'), null=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('last_updated', models.DateTimeField(auto_now=True)),
                ('amount', models.IntegerField(default=0)),
                ('from_currency', models.CharField(default='jpy', max_length=3)),
                ('to_currency', models.CharField(default='rmb', max_length=3)),
                ('due_at', models.DateTimeField(default=datetime.datetime(2019, 7, 31, 10, 33, 12, 131864))),
                ('rate', models.DecimalField(decimal_places=4, max_digits=10)),
                ('active', models.BooleanField(default=False)),
                ('status', models.CharField(default='new', max_length=10)),
                ('privacy', models.CharField(default='public', max_length=10)),
                ('send_notification', models.BooleanField(default=False)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='orders', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('-created',),
                'permissions': (('can_place_order', 'Can place order'), ('can_edit_order', 'Can edit order')),
            },
        ),
        migrations.CreateModel(
            name='Offer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('slug', models.SlugField(blank=True, default=uuid.UUID('36344660-5d8b-4020-9313-7d60eacd4a27'), null=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('last_updated', models.DateTimeField(auto_now=True)),
                ('price', models.IntegerField(default=0)),
                ('due_at', models.DateTimeField(default=datetime.datetime(2019, 7, 31, 10, 33, 12, 132899))),
                ('send_notification', models.BooleanField(default=False)),
                ('status', models.CharField(max_length=10)),
                ('follower', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='offers', to=settings.AUTH_USER_MODEL)),
                ('order', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='offers', to='mail_exchange.Order')),
            ],
            options={
                'ordering': ('-created',),
                'permissions': (('can_place_offer', 'Can place offer'), ('can_edit_offer', 'Can edit offer')),
            },
        ),
    ]
