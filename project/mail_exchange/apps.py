from django.apps import AppConfig


class MailExchangeConfig(AppConfig):
    name = 'mail_exchange'
