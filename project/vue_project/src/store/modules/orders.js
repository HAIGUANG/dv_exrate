import ordersAPI from '../../api/orders.js';

// initial state
const state = {
    orders:[]
};

// getters
const getters = {

    // chartdata_BocRates: (state, getters) =>{
    // },

};

// actions
const actions = {
    
    get_mailExchange_orders({ commit }) {
        console.log("get_mailExchange_orders");
        ordersAPI.get_public_orders(
            res => {
                console.log(res)
                commit("setMailExangeOrders",res.data);

            },err =>{
                console.log(err)
            }
        );
    },
};

const mutations = {
    setMailExangeOrders(state,data){
        state.orders=data;

    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
