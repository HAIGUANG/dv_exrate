import system from '../../api/system';

// initial state
const state = {
    todayrate:{},
    token:""
};

// getters
const getters = {

    // chartdata_BocRates: (state, getters) =>{
    // },

};

// actions
const actions = {
    
    getToken({ commit }) {
        console.log("get my token");
        system.getMyToken(
            res => {
                commit("setToken",res.data);
            },err =>{
                console.log(err)
            }
        );
    },
};

const mutations = {
    setToken(state,data){
        state.token=data.token;

    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
