
import Vue from 'vue';
import Router from 'vue-router';
Vue.use(Router);

import NewOrder from "../components/NewOrder.vue"



export default new Router({
    routes: [
        // {
        //     path: '/',
        //     name:"dashboard",
        //     components:{
        //         // inner_menu:InnerMenu,
        //         maincontent:MainContent
        //     }
        // },
        // {
        //     path: '/test',
        //     name:"test",
        //     components:{
        //         // inner_menu:InnerMenu,
        //         maincontent:TestContent
        //     }
        // },
        {
            path: '/new',
            name:"new",
            components:{
                // inner_menu:InnerMenu,
                maincontent:NewOrder
            }
        }
    ]
})