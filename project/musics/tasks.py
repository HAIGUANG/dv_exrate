from __future__ import absolute_import, unicode_literals
from celery import shared_task,task, chain, uuid
from django.core.mail import send_mail
from env_system.ShowapiRequest import ShowapiRequest
from django.core.cache import cache
import json
import decimal
from exrate.models import BankRate



@shared_task
def task_mail(message):
    subject = 'message from chat'
    message = 'message: {}'.format(message)
    mail_sent = send_mail(subject,
                          message,
                          'huhaiguang@me.com',
                          ['lionhu2009@gmail.com'])
    return mail_sent


@shared_task
def fetch_bankrate():
    subject = 'message from chat'
    r = ShowapiRequest("http://route.showapi.com/105-32","76812","ff6b110b4d884acf9d1ddc030a16c5a0" )
    r.addBodyPara("bankCode", "icbc")
    res = r.post()

    data=json.loads(res.text)

    fetch_error = data["showapi_res_error"]

    if not fetch_error:
      rates=data["showapi_res_body"]["codeList"]
      l_names = [d.get("name") for d in rates]

      jpy_index = l_names.index("日元")

      if jpy_index >-1:
        jpy_rate=rates[jpy_index]
        jpy_rate_db = {
            "name":jpy_rate["name"],
            "code":jpy_rate["code"],
            "hui_in":decimal.Decimal(jpy_rate["hui_in"]),
            "hui_out":decimal.Decimal(jpy_rate["hui_out"]),
            "chao_in":decimal.Decimal(jpy_rate["chao_in"]),
            "chao_out":decimal.Decimal(jpy_rate["chao_out"])
          }
        BankRate.objects.create(**jpy_rate_db)

        cache.set("todayrate",jpy_rate_db,3600)

        # >>> print(type(cache.get("bankrate")))
        # <class 'dict'>
        # >>> print(cache.get("bankrate")["name"])



    mail_sent = send_mail(subject,
                          json.dumps(jpy_rate, ensure_ascii=False),
                          'huhaiguang@me.com',
                          ['lionhu2009@gmail.com'])
    return mail_sent


@shared_task
def task_mail_chatmessage(message):
    subject = 'message from chat'
    message = 'message: {}'.format(message)
    mail_sent = send_mail(subject,
                          message,
                          'huhaiguang@me.com',
                          ['lionhu2009@gmail.com'])
    return mail_sent


@shared_task
def register_notification(email):
    subject = 'register_notification from Celery '
    message = 'User {} has been registered.'.format(email)
    mail_sent = send_mail(subject,
                          message,
                          'huhaiguang@me.com',
                          ['lionhu2009@gmail.com'])
    return mail_sent


@shared_task
def test_add(x, y):
    print("inside test_add")
    print(x)
    return x + y